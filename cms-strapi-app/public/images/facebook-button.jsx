import React from "react";

function Facebook() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100"
      height="100"
      version="1.2"
      viewBox="0 0 75 75"
    >
      <defs>
        <clipPath id="clip1">
          <path d="M2.828 2.828h69v69h-69zm0 0"></path>
        </clipPath>
      </defs>
      <g clipPath="url(#clip1)">
        <path
          fill="#3382A4"
          d="M37.328 2.96c-19.055 0-34.5 15.446-34.5 34.5 0 17.298 12.738 31.58 29.344 34.071v-24.93h-8.54v-9.066h8.54V31.5c0-9.992 4.867-14.379 13.168-14.379 3.976 0 6.082.297 7.078.43v7.918h-5.664c-3.524 0-4.758 3.34-4.758 7.11v4.956h10.332l-1.402 9.07h-8.93V71.61c16.84-2.289 29.832-16.683 29.832-34.148 0-19.055-15.445-34.5-34.5-34.5zm0 0"
        ></path>
      </g>
    </svg>
  );
}

export default Facebook;
