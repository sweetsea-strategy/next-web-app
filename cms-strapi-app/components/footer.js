import Container from "./container";
import { EXAMPLE_PATH } from "@/lib/constants";
import Facebook from "../public/images/facebook-button";
import PurpleWaveFull from "../public/images/purple-wave-full";
import Image from 'next/image';

export default function Footer() {
  return (
    <footer className="bg-accent-1 border-t border-accent-1">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
      >
        <path
          fill="#e2ebf3"
          fillOpacity="1"
          d="M0,128L48,117.3C96,107,192,85,288,85.3C384,85,480,107,576,101.3C672,96,768,64,864,64C960,64,1056,96,1152,117.3C1248,139,1344,149,1392,154.7L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>
      <div className="h-height300 flex footer-header bg-light-grey w-full">
        <div className="ml-16 mt-10 text-5xl md:text-6xl lg:text-7xl text-dark-blue font-serif">
          Explore More
        </div>
        <img
          className="absolute lg:ml-16 lg:mb-10"
          src="/images/wispy-footer-line.png"
        ></img>
      </div>
      <div className="footer-content bg-light-grey">
        <div className="footer-links-right text-dark-blue font-serif">
          <div className="mb-6">Home</div>
          <div className="mb-6">About</div>
          <div className="mb-6">Let's Work Together</div>
          <div className="mb-6">Learn Content Marketing</div>
          <div className="mb-6">Learn Search Engine Optimization</div>
        </div>
        <div className="footer-links-left text-dark-blue font-serif">
          <div className="mb-6">My Favorites</div>
          <div className="mb-6">Free Guides</div>
          <div className="mb-6">Blog Musings</div>
          <div className="mb-6">Contact</div>
        </div>
      </div>
      <div className="w-full flex flex-col items-center bg-light-grey">
        <div className="footer-wave">
        <Image src="/images/full-wave.png" alt="photo" width={500} height={200} layout="responsive" />
        </div>
        <div className="flex flex-row mb-10">
          <img className="sm:mx-8" src="/images/facebook-icon.png"></img>
          <img className="smLmx-8" src="/images/linkedIn-icon.png"></img>
          <img className="sm:mx-8" src="/images/pinterest-icon.png"></img>
        </div>
      </div>
      <div className="bg-dark-purple w-full h-12 flex-row flex justify-center">
        <div className="text-light-grey mt-2 text-2xl font-light font-serif">
          {" "}
          @SweetSea Digital LLC 2021 | Privacy Policy | Terms and conditions
        </div>
      </div>
    </footer>
  );
}
