import Image from "next/image";
import React, { useState, useEffect } from "react";

export default function Mobile({ intro, width, height }) {
  const x = width * 0.88
  const y = height * 0.90;
  return (
    <div className="flex relative">
      <div className="w-full flex relative flex-row">
        <div className="h-height550 w-w350 bg-sweetSea-darkBlue align-top"></div>
        <div className="absolute flex-none right-0">
          <Image
            src={
              process.env.NEXT_PUBLIC_STRAPI_API_URL +
              intro.profile.formats.medium.url
            }
            alt="intro.profile.alternativeText"
            width={x}
            height={y}
          />
        </div>
        <div className="bg-sweetSea-blue absolute right-0 w-9/12" style={{"marginTop":y * .95}}>
          <div className="text-center text-2xl p-2">{intro.title}</div>
          <div
            className="p-8 font-serif"
            dangerouslySetInnerHTML={{ __html: intro.body }}
          />
          <div className="text-center pb-10">
            <button className="inline-block px-6 py-2 leading-6 text-center bg-sweetSea-purple uppercase transition bg-transparent border-2 border-sweetSea-darkPurple rounded ripple hover:bg-sweetSea-lightPurple focus:outline-none">
              {intro.button_text}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
