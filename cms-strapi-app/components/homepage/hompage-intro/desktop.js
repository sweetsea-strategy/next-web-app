import Image from "next/image";
import PurpleWave from "../../../public/images/purple-wave";
import WispyFooterLine from "../../../public/images/wispy-footer-line";

export default function Desktop({ intro, width, height }) {
  const x = width > 1000 ? 500 : 350;
  const y = height * 0.8;
  return (
    <div className="relative">
      <div className="flex flex-col lg:flex-row w-full bg-light-grey">
        <div className="landing-1 flex flex-col items-center">
          <div className="img-container">
            <Image
              src={
                process.env.NEXT_PUBLIC_STRAPI_API_URL +
                intro.profile.formats.large.url
              }
              alt="intro.profile.alternativeText"
              width={384}
              height={600}
              layout="intrinsic"
            />
          </div>
        </div>
        <div className="landing-2 flex flex-col items-center ">
          <div
            id="landing-text"
            className="landing-text-section text-dark-purple"
          >
            <h1>Hi!</h1>
            <h2>I'm Seren, your SEO Sidekick.</h2>
            <h3>
              Getting conscious coaches & consultants found on Google to grow
              sustainably ...
            </h3>
            <h4>without the stress and spent hours.</h4>
          </div>
        </div>
      </div>
      <div className="svg-img">
        <Image
          src="/images/full-wave.png"
          alt="photo"
          width={200}
          height={100}
          layout="responsive"
        />
      </div>
    </div>
  );
}
