import Welcome from "../../public/images/welcome-circle";
import HoverHand from "../../public/images/hover-hand.jsx";

export default function HomepageTextBody({ textBody }) {
  //rename "how to" something along those lines
  return (
    <div className="w-full how-to-container flex bg-dark-blue">
      <div className="mt-auto mb-auto flex justify-center welcome">
        <div className="h-large w-w200 ml-20 welcome">
          <Welcome className="welcome" />
        </div>
      </div>
      <div className="how-to h-full w-full flex justify-center lg:justify-start">
        <div id="how-to-text" className="flex flex-col text-light-grey">
          <h1>Grab the 5 key steps to set your website up for SEO success</h1>
          <h2>I made it easy for you!</h2>
          {/* <h3>Free Download</h3> */}
          <button>I want This!</button>
          <div className="hover-hand">
            <HoverHand />
          </div>
        </div>
      </div>
    </div>
  );
}
