import Image from "next/image";
import FootprintsRight from "../../public/images/footprints-in-the-sand-right-side.jsx";

export default function HomepageFreebie({}) {
  return (
    <div>
      <div className="right-0 absolute footprint"></div>
      <div className="absolute footprint-left"></div>

      <div className="content-services">
        <div className="m-10 mt-72 lg:m-16 lg:mt-72 xl:w-6/12">
          <div className="text-center flex flex-col items-center">
            <div className="content-services-image">
              <Image
                src="/images/build-targeted-community.png"
                alt="photo"
                width={500}
                height={500}
                layout="responsive"
              />
            </div>
            <button className="content-service-btn bg-sandy-tan text-dark-blue">
              CONTENT MARKETING
            </button>
          </div>
        </div>
        <div className="mb-40 lg:mb-0 lg:m-16 lg:mt-72 xl:w-6/12">
          <div className="text-center flex flex-col items-center">
            <div className="content-services-image">
              <Image
                src="/images/grow-traffic-and-sales.png"
                alt="photo"
                width={500}
                height={500}
                layout="responsive"
              />
            </div>
            <button className="content-service-btn bg-sandy-tan text-dark-blue">
              SEARCH ENGINE OPTIMIZATION
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
