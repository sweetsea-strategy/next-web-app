import Image from "next/image";
import IconBlock from "./iconBlock";
import Integrity from "../../public/images/integrity.jsx";
export default function HomepageIconTextSection2({ iconBlock }) {
  return (
    <div>
      <div className="bg-light-grey flex-col text-dark-purple flex font-Madelyn text-9xl p-32 pb-0">
        <p>Blog Strategy +</p>
        <p>Sustainable traffic & lead Generation</p>
      </div>
      <div className="grey-beach flex-col flex">
        <div className="flex flex-col lg:flex-row justify-center lg:items-center items-center w-full lg:h-full">
          <div className="care flex flex-col items-center w-1/3">
            <Image
              src={"/images/care-values.png"}
              width={250}
              height={250}
              layout="fixed"
            />
          </div>
          <div className="ethical flex flex-col items-center w-1/3">
            <Image
              src={"/images/ethical-values.png"}
              width={250}
              height={250}
              layout="fixed"
            />
          </div>
          <div className="integrity flex flex-col items-center w-1/3">
            <Image
              src={"/images/integrity-values.png"}
              width={250}
              height={250}
              layout="fixed"
            />
          </div>
        </div>
        <div
          className="
        w-7/12 mx-auto text-dark-purple text-4xl my-14
        lg:text-5xl lg:mt-auto lg:mb-12"
        >
          <p className="text-right">
            Together, we unlock the potential of your website and set it up for
            growth - the kind of growth that compounds and boosts you business
            for years to come
          </p>
        </div>
        <div className="see-my-story">
          <Image src={"/images/see-my-story-button.png"} layout="fill" />
        </div>
      </div>
    </div>
  );
}
