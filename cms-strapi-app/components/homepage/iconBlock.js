import Image from "next/image";

export default function IconBlock({ iconName,heading,body,className }) {
  return (
    <div className={className}>
      <div className="text-center">
        <Image
          className="rounded-full"
          src={"/favicon/" + iconName}
          alt="title"
          width={150}
          height={150}
        />
      </div>

      <div className="text-2xl text-center flex-1">
        {heading}
      </div>
      <div
        className="p-8 text-center text-3xl flex-1"
        dangerouslySetInnerHTML={{ __html: body }}
      />
    </div>
  );
}
