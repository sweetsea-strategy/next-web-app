import Image from "next/image";

export default function FavoritesItems({ favorites }) {
  return (
    <div className="w-full bg-white h-full mt-14 font-serif">
      <div className="flex flex-col md:flex-row">
        <div className="flex flex-row-reverse w-full">
          <div className="w-w250 h-height600">
            <Image
              src={
                process.env.NEXT_PUBLIC_STRAPI_API_URL +
                "/uploads/SSD_Homepage_Pic_of_Me_6663ade8b0.png"
              }
              alt="photo"
              width={250}
              height={500}
              layout="responsive"
            />
          </div>
          <div className="mt-auto mb-auto w-6/12 h-large">
            <div className="text-5xl font-bold ml-10">Tools</div>
            <div className="mt-6 text-xl text-left ml-10">
              The tools i use to get my job done
            </div>
          </div>
        </div>
        <div className="flex flex-row w-full ml-10">
          <div className="w-w250 h-height600">
            <Image
              src={
                process.env.NEXT_PUBLIC_STRAPI_API_URL +
                "/uploads/SSD_Homepage_Pic_of_Me_6663ade8b0.png"
              }
              alt="photo"
              width={250}
              height={500}
              layout="responsive"
            />
          </div>
          <div className="mt-auto mb-auto w-6/12 h-large">
            <div className="text-5xl font-bold ml-10">Tools</div>
            <div className="mt-6 text-xl text-left ml-10">
              The tools i use to get my job done
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
