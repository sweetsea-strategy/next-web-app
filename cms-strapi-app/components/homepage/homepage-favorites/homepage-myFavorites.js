import Image from "next/image";
import FavoritesHeading from "./favourites-heading"
import FavoritesItems from "./favourite-items"

export default function HomepageMyFavorites({ favorites }) {
  return (
    <div className="">
          <FavoritesHeading />
          <FavoritesItems />
    </div>
  );
}
