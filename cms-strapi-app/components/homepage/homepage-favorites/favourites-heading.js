import Image from "next/image";

export default function FavoritesHeading({ favorites }) {
  return (
    <div className="bg-sweetSea-darkPurple h-full w-full flex flex-col">
      <div className="container flex flex-col ml-auto mr-auto">
        <div className="w-9/12 ml-auto mr-auto -mt-6 pr-4 md:max-w-2xl">
          <Image
            src={
              process.env.NEXT_PUBLIC_STRAPI_API_URL +
              "/uploads/SSD_Homepage_Pic_of_Me_6663ade8b0.png"
            }
            alt="photo"
            width={500}
            height={500}
            layout="responsive"
          />
        </div>
        <div className="w-7/12 ml-auto mr-auto text-left font-serif text-5xl text-white md:text-center">
          My Favorite Things
        </div>
        <div className="w-9/12 ml-auto mr-auto text-left font-mono text-sm text-white md:w-5/12 md:text-center">
          What does an entrepreneur / mom/ midwestern model come to love? Here’s
          where you find out! This is everything I have tested and loved, from
          my beauty holy-grails, to my ultimate tech and platforms that help me
          run my biz headache-free, to some pretty dang cute baby goodness, to
          how I dress myself to impress myself!
        </div>
        <button className="w-6/12 ml-auto mr-auto mt-10 mb-10 inline-block px-6 text-white py-2 leading-6 text-center bg-sweetSea-blue uppercase transition bg-transparent border-2 border-sweetSea-darkPurple rounded ripple focus:outline-none md:max-w-lg">
          Try it yourself
        </button>
      </div>
    </div>
  );
}
