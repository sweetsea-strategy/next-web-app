import Link from "next/link";
import Image from "next/image";

export default function HomepageInstagram({ instagramPosts }) {
  return (
    <>
      <div className="flex flex-col md:flex-row mt-48 text-dark-blue ml-2 md:ml-16">
        <div className="font-serif text-4xl md:text-5xl mr-4">
          On Instagram&trade;?
        </div>
        <div className="madelyn text-5xl md:text-6xl">I'd love to connect!</div>
      </div>
      <div className="instagram">
        {instagramPosts.map(({ node }, i) => {
          return (
            <a
              key={i}
              className="instagram-image"
              href={`https://www.instagram.com/p/${node.shortcode}`}
              aria-label="view image on Instagram"
            >
              <Image
                href={`https://www.instagram.com/p/${node.shortcode}`}
                key="{i}"
                src={node.thumbnail_src}
                alt={
                  // the caption with hashtags removed
                  node.edge_media_to_caption.edges[0].node.text
                    .replace(/(#\w+)+/g, "")
                    .trim()
                }
                width={400}
                height={400}
                layout="responsive"
              />
            </a>
          );
        })}
      </div>
    </>
  );
}
