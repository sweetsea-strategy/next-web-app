export default function HomepageLearning({  }) {
    return (
        <div>
            <div className="flex flex-col justify-center">
                <span className="text-center text-3xl font-serif font-bold">What do you want to learn today?</span>
                <div className="flex flex-col text-2xl p-10 items-center">
                    <span className="cursor-pointer hover:animate-bounce"> Content marketing</span>
                    <span className="cursor-pointer"> SEO </span>
                    <span className="cursor-pointer"> Latest musings</span>
                </div>
            </div>
        </div>
    );
  }