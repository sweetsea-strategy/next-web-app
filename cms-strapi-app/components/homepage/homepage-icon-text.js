import Image from "next/image";
import IconBlock from "./iconBlock";
import Integrity from "../../public/images/integrity.jsx";
export default function HomepageIconTextSection({ iconBlock }) {
  return (
    <div>
      <svg
        className="absolute"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
      >
        <path
          fill="#3382a4"
          fillOpacity="1"
          d="M0,32L26.7,26.7C53.3,21,107,11,160,32C213.3,53,267,107,320,117.3C373.3,128,427,96,480,90.7C533.3,85,587,107,640,138.7C693.3,171,747,213,800,197.3C853.3,181,907,107,960,80C1013.3,53,1067,75,1120,80C1173.3,85,1227,75,1280,80C1333.3,85,1387,107,1413,117.3L1440,128L1440,0L1413.3,0C1386.7,0,1333,0,1280,0C1226.7,0,1173,0,1120,0C1066.7,0,1013,0,960,0C906.7,0,853,0,800,0C746.7,0,693,0,640,0C586.7,0,533,0,480,0C426.7,0,373,0,320,0C266.7,0,213,0,160,0C106.7,0,53,0,27,0L0,0Z"
        ></path>
      </svg>
      <div className="beach flex-col flex">
        <div className="w-10/12 m-auto mt-96 flex flex-col items-start">
          <p className="my-6 text-dark-purple text-5xl font-serif">
            You're a coach, consultant or mentor determined to grow your impact
          </p>
          <p className="my-6 text-dark-purple text-5xl font-serif">
            Whether you're revamping your website, planning to launch digital
            products or a blog, or simply want more traffic to your offers, you
            don't want to spend hours learning SEO to think you're doing all the
            right things only to end up without the results you wanted
          </p>
        </div>
        {/* <div className="flex flex-col lg:flex-row justify-center lg:items-start items-center w-full h-full">
          <div className="care">
            <Image
              src={"/images/care-values.png"}
              width={250}
              height={250}
              layout="fixed"
            />
          </div>
          <div className="ethical">
            <Image
              src={"/images/ethical-values.png"}
              width={250}
              height={250}
              layout="fixed"
            />
          </div>
          <div className="integrity">
            <Image
              src={"/images/integrity-values.png"}
              width={250}
              height={250}
              layout="fixed"
            />
          </div>
        </div>
        <div className="see-my-story">
          <Image
            src={"/images/see-my-story-button.png"}
            layout="fill"
          />
        </div> */}
      </div>
    </div>
  );
}
