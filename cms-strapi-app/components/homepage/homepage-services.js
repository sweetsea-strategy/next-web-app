export default function HomepageServices({}) {
  return (
    <>
    <svg
    className="bg-light-purple"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 1440 320"
  >
    <path
      fill="#ffffff"
      fillOpacity="1"
      d="M0,256L48,234.7C96,213,192,171,288,165.3C384,160,480,192,576,224C672,256,768,288,864,266.7C960,245,1056,171,1152,149.3C1248,128,1344,160,1392,176L1440,192L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
    ></path>
  </svg>
    <div className="w-full flex justify-center bg-white lg:bg-white">
      <div className="services-container">
        <div className="text-6xl lg:text-7xl text-dark-blue font-serif ml-20">
          Not Ready Yet?
        </div>
        <div className="text-dark-blue no-feelings ">
          That's ok no hard feelings!
        </div>
        <p className="services-body text-dark-blue">
          Let's stay connected in the meantime so you can automatically get all
          the juicy SEO and content marketing bits.
        </p>
        <p className="services-body text-dark-blue">
          + Be the first to know about special offers, news, and brand new
          content designed to help you grow your business.
        </p>
        <div className="text-dark-blue no-feelings ">
          no spam/ excessive promotion/boring stuff.
        </div>
        <form className="mt-12 flex flex-col items-center" action="/">
          <input
            className="email-form bg-light-grey border-none text-5xl text-light-blue pb-6"
            type="text"
            id="fname"
            name="firstname"
            placeholder="First Name"
          />

          <input
            className="email-form mt-10 bg-light-grey border-none text-5xl text-light-blue pb-6"
            type="text"
            id="lname"
            name="email"
            placeholder="Your Best Email.."
          />
          <input
            className="email-form-btn bg-sandy-tan font-serif text-dark-blue"
            type="submit"
            value="SOUNDS GOOD I'M IN!"
          />
        </form>
      </div>
    </div>
    </>
  );
}
