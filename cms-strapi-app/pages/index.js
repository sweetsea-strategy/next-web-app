import dynamic from "next/dynamic";
import Instagram from "instagram-web-api";
import HomepageTextBody from "@/components/homepage/homepage-text-body";
import HomepageIconTextSection from "@/components/homepage/homepage-icon-text";
import HomepageMyFavorites from "@/components/homepage/homepage-favorites/homepage-myFavorites";
import HomepageLearning from "@/components/homepage/homepage-learning";
import HomepageIconTextSection2 from "@/components/homepage/homepage-icon-text copy";
import HomepageServices from "@/components/homepage/homepage-services";
import HomepageFreebie from "@/components/homepage/homepage-freebie";
import HomepageSeeServices from "@/components/homepage/homepage-see-services";
import HomepageInstagram from "@/components/homepage/homepage-instagram";
import Layout from "@/components/layout";
import Head from "next/head";
import { BUSINESS_NAME } from "@/lib/constants";
import { getHomepage } from "@/lib/api";

const HomepageIntro = dynamic(
  () => import("../components/homepage/hompage-intro/homepage-intro"),
  {
    ssr: false,
  }
);

export default function Index({ homepage, instagramPosts }) {
  var isReady = false;
  var width = 0;
  if (typeof window !== "undefined") {
    isReady = true;
    width = window.innerWidth;
  }
  return (
    <>
      <Layout>
        <Head>
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond&display=swap"
            rel="stylesheet"
          />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Oswald&display=swap"
            rel="stylesheet"
          />
          <title> Content Genius Seren Nurgun {BUSINESS_NAME}</title>
        </Head>
        <HomepageIntro intro={homepage.intro} />
        <HomepageTextBody textBody={homepage.textBody} />
        <HomepageIconTextSection iconBlock={homepage.iconBlock} />
        <HomepageIconTextSection2 iconBlock={homepage.iconBlock}/>
        <HomepageSeeServices />
        <HomepageFreebie /> 
        <HomepageServices />
        <HomepageInstagram instagramPosts={instagramPosts} />
        {/* <HomepageMyFavorites /> */}
        {/* <HomepageLearning />
        <HomepageServices />
        <HomepageFreebie /> */}
      </Layout>
    </>
  );
}

// export async function getStaticProps() {
//   const homepage = (await getHomepage()) || [];
//   return {
//     props: { homepage },
//   };
// }

export async function getStaticProps(context) {
  const client = new Instagram({
    username: "sutegy.2020",
    password: "SEren0107!",
  });

  let posts = [];
  let homepage = [];
  try {
    homepage = (await getHomepage()) || [];
    await client.login();
    // request photos for a specific instagram user
    const instagram = await client.getPhotosByUsername({
      username: "sweetseadigital",first:6
    });

    if (instagram["user"]["edge_owner_to_timeline_media"]["count"] > 0) {
      // if we receive timeline data back
      //  update the posts to be equal
      // to the edges that were returned from the instagram API response
      posts = instagram["user"]["edge_owner_to_timeline_media"]["edges"];
    }
  } catch (err) {
    console.log(
      "Something went wrong while fetching content from Instagram",
      err
    );
  }

  return {
    props: {
      instagramPosts: posts, // returns either [] or the edges returned from the Instagram API based on the response from the `getPhotosByUsername` API call
      homepage: homepage
    },
  };
}
