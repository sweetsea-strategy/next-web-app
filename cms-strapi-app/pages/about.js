import Layout from "@/components/layout";
import Head from "next/head";
import { BUSINESS_NAME } from "@/lib/constants";

export default function About() {
  return (
    <>
      <Layout>
        <Head>
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond&display=swap"
            rel="stylesheet"
          />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Oswald&display=swap"
            rel="stylesheet"
          />
          <title> Content Genius Seren Nurgun {BUSINESS_NAME}</title>
        </Head>
        <div className="w-full h-full bg-light-grey">
          <div className="flex flex-col w-full m-auto items-center">
            <div className="w-11/12 flex mt-14 p-10 flex-col border-t border-l border-double border-sweetSea-darkBlue">
              <p className="font-Madelyn text-8xl my-4 text-dark-purple">Welcome!</p>
              <p className="font-serif font-bold text-7xl text-dark-blue w-10/12">Growing websites with traffic-building content & optimizations is my specialty.</p>
              <p className="font-serif text-5xl my-10 text-dark-blue w-10/12">I'm Seren (she/her), a search engine optimization and content marketing strategist for conscious coaches, consultants and mentors.</p>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
}
