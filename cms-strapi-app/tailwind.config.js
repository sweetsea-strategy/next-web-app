const { lightBlue } = require("tailwindcss/colors");
const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./components/**/*.js", "./pages/**/*.js"],
  theme: {
    extend: {
      fontFamily: {
        serif: ["Cormorant Garamond"],
        'display': ['Oswald'],
        'Madelyn': ["Madelyn","sans-serif"]
      },
      colors: {
        "accent-1": "#FAFAFA",
        "accent-2": "#EAEAEA",
        "accent-7": "#333",
        success: "#0070f3",
        transparent: "transparent",
        current: "currentColor",
        cyan: colors.cyan,
        "sweetSea-lightBlue": "#E1DCD7",
        "sweetSea-blue": "#7698C1",
        "sweetSea-darkBlue": "#0E7490",
        "sweetSea-lightPurple": "#E1DCD7",
        "sweetSea-darkPurple": "#A5BEE4",
        "sweetSea-purple": "#3382A4",
        "light-grey": "#e2ebf3",
        "dark-purple": "#526fa2",
        "dark-blue": "#3382a4",
        "sandy-tan": "#e1dcd7",
        "light-blue": "#b1d7e7",
        "light-purple": "#a5bee4",

      },
      spacing: {
        28: "7rem",
      },
      letterSpacing: {
        tighter: "-.04em",
      },
      lineHeight: {
        tight: 1.2,
      },
      fontSize: {
        "5xl": "2.5rem",
        "6xl": "2.75rem",
        "7xl": "4.5rem",
        "8xl": "6.25rem",
      },
      boxShadow: {
        small: "0 5px 10px rgba(0, 0, 0, 0.12)",
        medium: "0 8px 30px rgba(0, 0, 0, 0.12)",
      },
      height: {
        large: "200px",
        height300: "300px",
        large2Xl: "400px",
        height500:"500px",
        height550:"550px",
        height600:"600px",
        height800:"800px",
        height700:"700px",
        large3Xl: "1000px",
        large4Xl: "1200px",
        large5Xl: "1600px",
        large6Xl: "2000px",

      },
      width: {
        w200:"200px",
        w250:"250px",
        w300:"300px",
        w350: "350px",
        w400: "400px",
        w450: "450px",
        w500: "500px",
        w550: "550px",
        w600: "600px",
        w650: "650px",
        w700: "700px",
        w750: "750px",
        w800: "800px",
      },
      margin: {
        large25:"25rem",
        large29:"29rem",
        large30:"30rem",
        large40:"40rem",
        large: "20rem",
        Percent20: "20%",
        Percent40: "40%",
        Percent50: "50%"
      },
    },
  },
  variants: {
    extend: {
     animation: ['hover', 'focus'],
    }
  }
};
